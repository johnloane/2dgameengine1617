var SnailBait = function()
{
	this.canvas = document.getElementById('game-canvas'),
	this.context = this.canvas.getContext('2d'),
	this.fpsElement = document.getElementById('fps'),
	this.toastElement = document.getElementById('toast'),
	this.instructionElement = document.getElementById('instructions'),
	this.copyrightElement = document.getElementById('copyright'),
	this.scoreElement = document.getElementById('score'),
	this.soundAndMusicElement = document.getElementById('sound-and-music'),
	this.loadingElement = document.getElementById('loading'),
	this.loadingTitleElement = document.getElementById('loading-title'),
	this.loadingAnimatedGIFElement = document.getElementById('loading-animated-gif'),

  //Music
  this.musicElement = document.getElementById('snailbait-music');
  this.musicElement.volume = 0.1;
  this.musicCheckboxElement = document.getElementById('music-checkbox');
  this.musicOn = this.musicCheckboxElement.checked;

  //Sound
  this.soundCheckboxElement = document.getElementById('sound-checkbox');
  this.audioSprites = document.getElementById('snailbait-audio-sprites');
  this.soundOn = this.soundCheckboxElement.checked;

  // Mobile............................................................

   this.mobileInstructionsVisible = false;

   this.mobileStartToast = 
      document.getElementById('snailbait-mobile-start-toast');

   this.mobileWelcomeToast = 
      document.getElementById('snailbait-mobile-welcome-toast');

   this.welcomeStartLink = 
      document.getElementById('snailbait-welcome-start-link');

   this.showHowLink = 
      document.getElementById('snailbait-show-how-link');

   this.mobileStartLink = 
      document.getElementById('snailbait-mobile-start-link');

  //Time
  this.playing = true;
  this.timeSystem = new TimeSystem();
  this.timeRate = 1.0; //1.0 is normal time, 0.5 1/2 speed

  //Pixels and meters
  this.CANVAS_WIDTH_IN_METRES = 13,
  this.PIXELS_PER_METRE = this.canvas.width/this.CANVAS_WIDTH_IN_METRES;

  //Gravity
  this.GRAVITY_FORCE = 9.81;

	//Constants
  this.RIGHT = 1,
  this.LEFT = 2,
  this.BUTTON_PACE_VELOCITY = 80,
	this.RUNNER_LEFT = 50,
	this.PLATFORM_HEIGHT = 8,
	this.PLATFORM_STROKE_WIDTH = 2,
	this.PLATFORM_STROKE_STYLE = 'rgb(0,0,0)',

  this.RUNNER_EXPOSION_DURATION = 500,
  this.BAD_GUYS_EXPLOSION_DURATION = 1500,

	this.STARTING_RUNNER_TRACK = 1,

	this.TRACK_1_BASELINE = 323,
	this.TRACK_2_BASELINE = 223,
	this.TRACK_3_BASELINE = 123,

	this.SHORT_DELAY = 50,
	this.OPAQUE = 1.0,
	this.TRANSPARENT = 0,

	//Game states
	this.paused = false,
	this.PAUSED_CHECK_INTERVAL = 200, //time in milliseconds
	this.pauseStartTime,
	this.windowHasFocus = true,
	this.countDownInProgress = false,
	this.gameStarted = false,
  this.playing = false,

	//Images
	this.background = new Image(),
	this.runner = new Image(),
	this.spritesheet = new Image(),
	//this.loadingAnimatedGIFElement = new Image();

	//Time
	this.lastAnimationFrameTime = 0,
	this.lastFpsUpdateTime = 0,
	this.fps = 60;

	//Scrolling
	this.STARTING_BACKGROUND_OFFSET = 0,
	this.STARTING_BACKGROUND_VELOCITY = 0,
	this.BACKGROUND_VELOCITY = 25,
	this.STARTING_PLATFORM_OFFSET = 0,
	this.PLATFORM_VELOCITY_MULTIPLER = 4.35,
	this.STARTING_SPRITE_OFFSET = 0,

	//Animation
	this.RUN_ANIMATION_RATE = 14,


	//Translation offsets
	this.backgroundOffset = this.STARTING_BACKGROUND_OFFSET,
	this.spriteOffset = this.STARTING_SPRITE_OFFSET,
	this.bgVelocity = this.STARTING_BACKGROUND_VELOCITY,
	this.platformOffset = this.STARTING_PLATFORM_OFFSET,
	this.platformVelocity;

  // Background width and height.......................................

   this.BACKGROUND_WIDTH = 1102;
   this.BACKGROUND_HEIGHT = 400;

	//Key codes
	this.KEY_D = 68,
	this.KEY_LEFT = 37,
	this.KEY_K = 75,
	this.KEY_RIGHT = 39,
	this.KEY_P = 80,

 // Sprite sheet cells................................................

   this.RUNNER_CELLS_WIDTH = 50; // pixels
   this.RUNNER_CELLS_HEIGHT = 54;

   this.BAT_CELLS_HEIGHT = 34; // Bat cell width varies; not constant 

   this.BEE_CELLS_HEIGHT = 50;
   this.BEE_CELLS_WIDTH  = 50;

   this.BUTTON_CELLS_HEIGHT  = 20;
   this.BUTTON_CELLS_WIDTH   = 31;

   this.COIN_CELLS_HEIGHT = 30;
   this.COIN_CELLS_WIDTH  = 30;
   
   this.EXPLOSION_CELLS_HEIGHT = 62;

   this.RUBY_CELLS_HEIGHT = 30;
   this.RUBY_CELLS_WIDTH = 35;

   this.SAPPHIRE_CELLS_HEIGHT = 30;
   this.SAPPHIRE_CELLS_WIDTH  = 35;

   this.SNAIL_BOMB_CELLS_HEIGHT = 20;
   this.SNAIL_BOMB_CELLS_WIDTH  = 20;

   this.SNAIL_CELLS_HEIGHT = 34;
   this.SNAIL_CELLS_WIDTH  = 64;
   this.SNAIL_PACE_VELOCITY = 80;

   this.batCells = [
      { left: 3,   top: 0, width: 36, height: this.BAT_CELLS_HEIGHT },
      { left: 41,  top: 0, width: 46, height: this.BAT_CELLS_HEIGHT },
      { left: 93,  top: 0, width: 36, height: this.BAT_CELLS_HEIGHT },
      { left: 132, top: 0, width: 46, height: this.BAT_CELLS_HEIGHT },
   ];

   this.batRedEyeCells = [
      { left: 185, top: 0, 
        width: 36, height: this.BAT_CELLS_HEIGHT },

      { left: 222, top: 0, 
        width: 46, height: this.BAT_CELLS_HEIGHT },

      { left: 273, top: 0, 
        width: 36, height: this.BAT_CELLS_HEIGHT },

      { left: 313, top: 0, 
        width: 46, height: this.BAT_CELLS_HEIGHT },
   ];
   
   this.beeCells = [
      { left: 5,   top: 234, width: this.BEE_CELLS_WIDTH,
                            height: this.BEE_CELLS_HEIGHT },

      { left: 75,  top: 234, width: this.BEE_CELLS_WIDTH, 
                            height: this.BEE_CELLS_HEIGHT },

      { left: 145, top: 234, width: this.BEE_CELLS_WIDTH, 
                            height: this.BEE_CELLS_HEIGHT }
   ];
   
   this.blueCoinCells = [
      { left: 5, top: 540, width: this.COIN_CELLS_WIDTH, 
                           height: this.COIN_CELLS_HEIGHT },

      { left: 5 + this.COIN_CELLS_WIDTH, top: 540,
        width: this.COIN_CELLS_WIDTH, 
        height: this.COIN_CELLS_HEIGHT }
   ];

   this.explosionCells = [
      { left: 3,   top: 48, 
        width: 52, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 63,  top: 48, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 146, top: 48, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 233, top: 48, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 308, top: 48, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 392, top: 48, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 473, top: 48, 
        width: 70, height: this.EXPLOSION_CELLS_HEIGHT }
   ];

   // Sprite sheet cells................................................

   this.blueButtonCells = [
      { left: 10,   top: 192, width: this.BUTTON_CELLS_WIDTH,
                            height: this.BUTTON_CELLS_HEIGHT },

      { left: 53,  top: 192, width: this.BUTTON_CELLS_WIDTH, 
                            height: this.BUTTON_CELLS_HEIGHT }
   ];

   this.goldCoinCells = [
      { left: 65, top: 540, width: this.COIN_CELLS_WIDTH, 
                            height: this.COIN_CELLS_HEIGHT },
      { left: 96, top: 540, width: this.COIN_CELLS_WIDTH, 
                            height: this.COIN_CELLS_HEIGHT },
      { left: 128, top: 540, width: this.COIN_CELLS_WIDTH, 
                             height: this.COIN_CELLS_HEIGHT },
   ];

   this.goldButtonCells = [
      { left: 90,   top: 190, width: this.BUTTON_CELLS_WIDTH,
                              height: this.BUTTON_CELLS_HEIGHT },

      { left: 132,  top: 190, width: this.BUTTON_CELLS_WIDTH,
                              height: this.BUTTON_CELLS_HEIGHT }
   ];

   this.rubyCells = [
      { left: 3,   top: 138, width: this.RUBY_CELLS_WIDTH,
                             height: this.RUBY_CELLS_HEIGHT },

      { left: 39,  top: 138, width: this.RUBY_CELLS_WIDTH, 
                             height: this.RUBY_CELLS_HEIGHT },

      { left: 76,  top: 138, width: this.RUBY_CELLS_WIDTH, 
                             height: this.RUBY_CELLS_HEIGHT },

      { left: 112, top: 138, width: this.RUBY_CELLS_WIDTH, 
                             height: this.RUBY_CELLS_HEIGHT },

      { left: 148, top: 138, width: this.RUBY_CELLS_WIDTH, 
                             height: this.RUBY_CELLS_HEIGHT }
   ];

   this.runnerCellsRight = [
      { left: 414, top: 385, 
        width: 47, height: this.RUNNER_CELLS_HEIGHT },

      { left: 362, top: 385, 
         width: 44, height: this.RUNNER_CELLS_HEIGHT },

      { left: 314, top: 385, 
         width: 39, height: this.RUNNER_CELLS_HEIGHT },

      { left: 265, top: 385, 
         width: 46, height: this.RUNNER_CELLS_HEIGHT },

      { left: 205, top: 385, 
         width: 49, height: this.RUNNER_CELLS_HEIGHT },

      { left: 150, top: 385, 
         width: 46, height: this.RUNNER_CELLS_HEIGHT },

      { left: 96,  top: 385, 
         width: 46, height: this.RUNNER_CELLS_HEIGHT },

      { left: 45,  top: 385, 
         width: 35, height: this.RUNNER_CELLS_HEIGHT },

      { left: 0,   top: 385, 
         width: 35, height: this.RUNNER_CELLS_HEIGHT }
   ],

   this.runnerCellsLeft = [
      { left: 0,   top: 305, 
         width: 47, height: this.RUNNER_CELLS_HEIGHT },

      { left: 55,  top: 305, 
         width: 44, height: this.RUNNER_CELLS_HEIGHT },

      { left: 107, top: 305, 
         width: 39, height: this.RUNNER_CELLS_HEIGHT },

      { left: 152, top: 305, 
         width: 46, height: this.RUNNER_CELLS_HEIGHT },

      { left: 208, top: 305, 
         width: 49, height: this.RUNNER_CELLS_HEIGHT },

      { left: 265, top: 305, 
         width: 46, height: this.RUNNER_CELLS_HEIGHT },

      { left: 320, top: 305, 
         width: 42, height: this.RUNNER_CELLS_HEIGHT },

      { left: 380, top: 305, 
         width: 35, height: this.RUNNER_CELLS_HEIGHT },

      { left: 425, top: 305, 
         width: 35, height: this.RUNNER_CELLS_HEIGHT },
   ],

   this.sapphireCells = [
      { left: 185,   top: 138, width: this.SAPPHIRE_CELLS_WIDTH,
                             height: this.SAPPHIRE_CELLS_HEIGHT },

      { left: 220,  top: 138, width: this.SAPPHIRE_CELLS_WIDTH, 
                             height: this.SAPPHIRE_CELLS_HEIGHT },

      { left: 258,  top: 138, width: this.SAPPHIRE_CELLS_WIDTH, 
                             height: this.SAPPHIRE_CELLS_HEIGHT },

      { left: 294, top: 138, width: this.SAPPHIRE_CELLS_WIDTH, 
                             height: this.SAPPHIRE_CELLS_HEIGHT },

      { left: 331, top: 138, width: this.SAPPHIRE_CELLS_WIDTH, 
                             height: this.SAPPHIRE_CELLS_HEIGHT }
   ];

   this.snailBombCells = [
      { left: 40, top: 512, width: 30, height: 20 },
      { left: 2, top: 512, width: 30, height: 20 }
   ];

   this.snailCells = [
      { left: 142, top: 466, width: this.SNAIL_CELLS_WIDTH,
                             height: this.SNAIL_CELLS_HEIGHT },

      { left: 75,  top: 466, width: this.SNAIL_CELLS_WIDTH, 
                             height: this.SNAIL_CELLS_HEIGHT },

      { left: 2,   top: 466, width: this.SNAIL_CELLS_WIDTH, 
                             height: this.SNAIL_CELLS_HEIGHT },
   ]; 

   // Sprite data.......................................................
  

   this.batData = [
      { left: 95,  
         top: this.TRACK_2_BASELINE - 1.5*this.BAT_CELLS_HEIGHT },

      { left: 614,  
         top: this.TRACK_3_BASELINE },

      { left: 904,  
         top: this.TRACK_3_BASELINE - 3*this.BAT_CELLS_HEIGHT },

      { left: 1180, 
         top: this.TRACK_2_BASELINE - 2.5*this.BAT_CELLS_HEIGHT },

      { left: 1720, 
         top: this.TRACK_2_BASELINE - 2*this.BAT_CELLS_HEIGHT },

      { left: 1960, 
         top: this.TRACK_3_BASELINE - this.BAT_CELLS_HEIGHT }, 

      { left: 2200, 
         top: this.TRACK_3_BASELINE - this.BAT_CELLS_HEIGHT },

      { left: 2380, 
         top: this.TRACK_3_BASELINE - 2*this.BAT_CELLS_HEIGHT }
   ];
   
   this.beeData = [
      { left: 225,  
         top: this.TRACK_1_BASELINE - this.BEE_CELLS_HEIGHT*1.25 },
      { left: 355,  
         top: this.TRACK_2_BASELINE - this.BEE_CELLS_HEIGHT*1.25 },
      { left: 520,  
         top: this.TRACK_1_BASELINE - this.BEE_CELLS_HEIGHT },
      { left: 780,  
         top: this.TRACK_1_BASELINE - this.BEE_CELLS_HEIGHT*1.25 },

      { left: 924,  
         top: this.TRACK_2_BASELINE - this.BEE_CELLS_HEIGHT*1.25 },

      { left: 1500, top: 225 },
      { left: 1600, top: 115 },
      { left: 2225, top: 125 },
      { left: 2195, top: 275 },
      { left: 2450, top: 275 }
   ];
   
   this.buttonData = [
      { platformIndex: 7 },
      { platformIndex: 12 }
   ];

   this.coinData = [
      { left: 270,  
         top: this.TRACK_2_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 489,  
         top: this.TRACK_3_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 620,  
         top: this.TRACK_1_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 833,  
         top: this.TRACK_2_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 1050, 
         top: this.TRACK_2_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 1450, 
         top: this.TRACK_1_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 1670, 
         top: this.TRACK_2_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 1870, 
         top: this.TRACK_1_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 1930, 
         top: this.TRACK_1_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 2200, 
         top: this.TRACK_2_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 2320, 
         top: this.TRACK_2_BASELINE - this.COIN_CELLS_HEIGHT }, 

      { left: 2360, 
         top: this.TRACK_1_BASELINE - this.COIN_CELLS_HEIGHT }
   ];  

   // Platforms.........................................................

   this.platformData = [
      // Screen 1.......................................................
      {
         left:      10,
         width:     210,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(200, 200, 60)',
         opacity:   1.0,
         track:     1,
         pulsate:   false,
      },

      {  left:      240,
         width:     110,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(110,150,255)',
         opacity:   1.0,
         track:     2,
         pulsate:   false,
      },

      {  left:      400,
         width:     125,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(250,0,0)',
         opacity:   1.0,
         track:     3,
         pulsate:   false
      },

      {  left:      603,
         width:     250,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(255,255,0)',
         opacity:   0.8,
         track:     1,
         pulsate:   false,
      },

      // Screen 2.......................................................
               
      {  left:      810,
         width:     100,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(200,200,0)',
         opacity:   1.0,
         track:     2,
         pulsate:   false
      },

      {  left:      1005,
         width:     150,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(80,140,230)',
         opacity:   1.0,
         track:     2,
         pulsate:   false
      },

      {  left:      1200,
         width:     105,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'aqua',
         opacity:   1.0,
         track:     3,
         pulsate:   false
      },

      {  left:      1400,
         width:     180,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'aqua',
         opacity:   1.0,
         track:     1,
         pulsate:   false,
      },

      // Screen 3.......................................................
               
      {  left:      1625,
         width:     100,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'cornflowerblue',
         opacity:   1.0,
         track:     2,
         pulsate:   false
      },

      {  left:      1800,
         width:     250,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'gold',
         opacity:   1.0,
         track:     1,
         pulsate:   false
      },

      {  left:      2000,
         width:     200,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'rgb(200,200,80)',
         opacity:   1.0,
         track:     2,
         pulsate:   false
      },

      {  left:      2100,
         width:     100,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'aqua',
         opacity:   1.0,
         track:     3,
         pulsate:   false
      },


      // Screen 4.......................................................

      {  left:      2269,
         width:     200,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: 'gold',
         opacity:   1.0,
         track:     1,
         pulsate:   true
      },

      {  left:      2500,
         width:     200,
         height:    this.PLATFORM_HEIGHT,
         fillStyle: '#2b950a',
         opacity:   1.0,
         track:     2,
         pulsate:   true
      }
   ];

   this.sapphireData = [
      { left: 155,  
         top: this.TRACK_1_BASELINE - this.SAPPHIRE_CELLS_HEIGHT },

      { left: 880,  
         top: this.TRACK_2_BASELINE - this.SAPPHIRE_CELLS_HEIGHT },

      { left: 1100, 
         top: this.TRACK_2_BASELINE - this.SAPPHIRE_CELLS_HEIGHT }, 

      { left: 1475, 
         top: this.TRACK_1_BASELINE - this.SAPPHIRE_CELLS_HEIGHT },

      { left: 2400, 
         top: this.TRACK_1_BASELINE - this.SAPPHIRE_CELLS_HEIGHT }
   ];

   this.rubyData = [
      { left: 690,  
         top: this.TRACK_1_BASELINE - this.RUBY_CELLS_HEIGHT },

      { left: 1700, 
         top: this.TRACK_2_BASELINE - this.RUBY_CELLS_HEIGHT },

      { left: 2056, 
         top: this.TRACK_2_BASELINE - this.RUBY_CELLS_HEIGHT }
   ];

   this.smokingHoleData = [
      { left: 250,  top: this.TRACK_2_BASELINE - 20 },
      { left: 850,  top: this.TRACK_2_BASELINE - 20 }
   ];
   
   this.snailData = [
      { platformIndex: 13 },
   ];
  //Sounds
  this.cannonSound = {
    position: 7.7, //seconds
    duration: 1031, //milliseconds
    volume: 0.5
  };

  this.coinSound = {
    position: 7.1, //seconds
    duration: 588, //milliseconds
    volume: 0.5
  };

  this.electricityFlowingSound = {
    position: 1.03, //seconds
    duration: 1753, //milliseconds
    volume: 0.5
  };

  this.explosionSound = {
    position: 4.3, //seconds
    duration: 760, //milliseconds
    volume: 1.0
  };

  this.pianoSound = {
    position: 5.6, //seconds
    duration: 395, //milliseconds
    volume: 0.5
  };

  this.thudSound = {
    position: 3.1, //seconds
    duration: 809, //milliseconds
    volume: 1.0
  };

  this.audioChannels = [ //4 channels
    {playing:false, audio: this.audioSprites},
    {playing:false, audio: null},
    {playing:false, audio: null},
    {playing:false, audio: null},
    ];

  this.audioSpriteCountdown = this.audioChannels.length - 1;
  this.graphicsReady = false;

    //Sprite Behaviours
    this.runBehaviour = 
    {
      lastAdvanceTime: 0,

      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
      	if(sprite.runAnimationRate === 0)
      	{
      		return;
      	}
      	if(this.lastAdvanceTime === 0)
      	{
      		this.lastAdvanceTime = now;
      	}
      	else if(now - this.lastAdvanceTime > 1000/sprite.runAnimationRate)
      	{
      		sprite.artist.advance();
      		this.lastAdvanceTime = now;
      	}
      }
    };

    this.paceBehaviour = {
      setDirection: function(sprite)
      {
        var sRight = sprite.left + sprite.width,
            pRight = sprite.platform.left + sprite.platform.width;

        if(sprite.direction === undefined)
        {
          sprite.direction = snailBait.RIGHT;
        }
        if(sRight > pRight && sprite.direction === snailBait.RIGHT)
        {
          sprite.direction = snailBait.LEFT;
        }
        else if(sprite.left < sprite.platform.left && sprite.direction === snailBait.LEFT)
        {
          sprite.direction = snailBait.RIGHT;
        }
      },

      setPosition: function(sprite, now, lastAnimationFrameTime)
      {
        //console.log(sprite.velocityX);
        var pixelsToMove = sprite.velocityX * (now - lastAnimationFrameTime)/1000;
        //console.log(pixelsToMove);

        if(sprite.direction === snailBait.RIGHT)
        {
          sprite.left += pixelsToMove;
        }
        else
        {
          sprite.left -= pixelsToMove;
        }
      },

      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
        //console.log("Pace Behaviour Execute being called");
        this.setDirection(sprite);
        this.setPosition(sprite, now, lastAnimationFrameTime);
      }
    };

    //Snail Shoot Behaviour
    this.snailShootBehaviour = 
    {
      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
        var bomb = sprite.bomb,
            MOUTH_OPEN_CELL = 2;

        if(!snailBait.isSpriteInView(sprite))
        {
          return;
        }
        if(!bomb.visible && sprite.artist.cellIndex === MOUTH_OPEN_CELL)
        {
          bomb.left = sprite.left;
          bomb.visible = true;
          snailBait.playSound(snailBait.cannonSound);
        }
      }
    };

    //Move the snail bomb
    this.snailBombMoveBehaviour = 
    {
      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
        var SNAIL_BOMB_VELOCITY = 250;
        if(sprite.left + sprite.width > sprite.hOffset && sprite.left + sprite.width < sprite.hOffset + sprite.width)
        {
          sprite.visible = false;
        }
        else
        {
          sprite.left -= SNAIL_BOMB_VELOCITY * ((now-lastAnimationFrameTime)/1000);
        }
      }
    };

    //Runner Jump Behaviour
    this.jumpBehaviour = 
    {
      pause: function(sprite, now)
      {
        if(sprite.ascendTimer.isRunning())
        {
          sprite.ascendTimer.pause(now);
        }
        else if(sprite.descendTimer.isRunning())
        {
          sprite.descendTimer.pause(now);
        }
      },

      unpause: function(sprite, now)
      {
        if(sprite.ascendTimer.isRunning())
        {
          sprite.ascendTimer.unpause(now);
        }
        else if(sprite.descendTimer.isRunning())
        {
          sprite.descendTimer.unpause(now);
        }
      },

      isAscending: function(sprite)
      {
        return sprite.ascendTimer.isRunning();
      },

      ascend: function(sprite, now)
      {
        var elapsedTime = sprite.ascendTimer.getElapsedTime(now),
            deltaY = elapsedTime/(sprite.JUMP_DURATION/2) * sprite.JUMP_HEIGHT;
        sprite.top = sprite.verticalLaunchPosition - deltaY;
      },

      isDoneAscending: function(sprite, now)
      {
        return sprite.ascendTimer.getElapsedTime(now) > sprite.JUMP_DURATION/2;
      },

      finishAscent: function(sprite, now)
      {
        sprite.jumpApex = sprite.top;
        sprite.ascendTimer.stop(now);
        sprite.descendTimer.start(now);
      },

      isDescending: function(sprite)
      {
        return sprite.descendTimer.isRunning();
      },

      descend: function(sprite, now)
      {
        var elapsedTime = sprite.descendTimer.getElapsedTime(now),
            deltaY = elapsedTime/(sprite.JUMP_DURATION/2) * sprite.JUMP_HEIGHT;
        sprite.top = sprite.jumpApex + deltaY;
      },

      isDoneDescending: function(sprite, now)
      {
        return sprite.descendTimer.getElapsedTime(now) > sprite.JUMP_DURATION/2;
      },

      finishDescent: function(sprite, now)
      {
        sprite.stopJumping();
        if(snailBait.platformUnderneath(sprite))
        {
          sprite.top = sprite.verticalLaunchPosition;
        }
        else
        {
          sprite.fall(snailBait.GRAVITY_FORCE *
               (sprite.descendTimer.getElapsedTime(now)/1000) *
               snailBait.PIXELS_PER_METER);
        }
        //sprite.runAnimationRate = snailBait.RUN_ANIMATION_RATE;
      },

      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
        if(!sprite.jumping || sprite.falling)
        {
          return;
        }
        if(this.isAscending(sprite))
        {
          if(!this.isDoneAscending(sprite, now))
          {
            this.ascend(sprite, now);
          }
          else
          {
            this.finishAscent(sprite, now);
          }
        }
        else if(this.isDescending(sprite))
        {
          if(!this.isDoneDescending(sprite, now))
          {
            this.descend(sprite, now);
          }
          else 
          {
            this.finishDescent(sprite, now);
          }
        }
      }
    };

    this.collideBehaviour={
      //Three step process
      //1. What should we consider for collision?
      //2. Is there a collision
      //3. React to the collision
      isCandidateForCollision: function(sprite, otherSprite)
      {
        var s, o;
        s = sprite.calculateCollisionRectangle(),
        o = otherSprite.calculateCollisionRectangle();

        candidate =  o.left < s.right && sprite !== otherSprite &&
        sprite.visible && otherSprite.visible && !sprite.exploding && !otherSprite.exploding;
        return candidate;
      },

      didCollide: function(sprite, otherSprite, context)
      {
        var o = otherSprite.calculateCollisionRectangle(),
            r = sprite.calculateCollisionRectangle();
        if (otherSprite.type === 'snail bomb') {
            return this.didRunnerCollideWithSnailBomb(r, o, context);
         }
         else {
            return this.didRunnerCollideWithOtherSprite(r, o, context);
         }

      },

      didRunnerCollideWithSnailBomb: function (r, o, context) {
         // Determine if the center of the snail bomb lies
         // within the runner's bounding box.

         context.beginPath();
         context.rect(r.left + snailBait.spriteOffset, 
                      r.top, r.right - r.left, r.bottom - r.top);

         return context.isPointInPath(o.centerX, o.centerY);
      },

      didRunnerCollideWithOtherSprite: function (r, o, context) {
         // Determine if either of the runner's four corners or its
         // center lie within the other sprite's bounding box.

         context.beginPath();
         context.rect(o.left, o.top, o.right - o.left, o.bottom - o.top);

         return context.isPointInPath(r.left,  r.top)       ||
                context.isPointInPath(r.right, r.top)       ||

                context.isPointInPath(r.centerX, r.centerY) ||

                context.isPointInPath(r.left,  r.bottom)    ||
                context.isPointInPath(r.right, r.bottom);
      },

      processPlatformCollisionDuringJump: function(sprite, platform)
      {
        var isDescending = sprite.descendTimer.isRunning();
        sprite.stopJumping();
        if(isDescending)
        {
          sprite.track = platform.track;
          sprite.top = snailBait.calculatePlatformTop(sprite.track) - sprite.height;
        }
        else
        {
          sprite.fall();
          snailBait.playSound(snailBait.thudSound);
        }
      },

      processBadGuyCollision: function(sprite)
      {
         snailBait.runner.stopFalling();
         snailBait.runner.stopJumping();
         snailBait.explode(sprite);
         snailBait.shake();
         snailBait.loseLife();
      },

      processCollision: function(sprite, otherSprite)
      {
        if(sprite.jumping && 'platform' === otherSprite.type)
        {
          //console.log("Jump platform collision");
          this.processPlatformCollisionDuringJump(sprite, otherSprite);
        }
        else if('coin' === otherSprite.type || 'sapphire' === otherSprite.type || 'ruby' === otherSprite.type)
        {
          //console.log("Good collision");
          this.processAssetCollision(otherSprite);
        }
        else if('bat' === otherSprite.type || 'bee' === otherSprite.type || 'snail bomb' === otherSprite.type)
        {
          //console.log("Bad collision");
          this.processBadGuyCollision(sprite);
        }
        else if('button' === otherSprite.type)
        {
          if(sprite.jumping && sprite.descendTimer.isRunning() || sprite.falling)
          {
            otherSprite.detonating = true;
          }
        }
      },

      processAssetCollision: function (sprite) {
         sprite.visible = false; // sprite is the asset

         if (sprite.type === 'coin') {
            snailBait.playSound(snailBait.coinSound);
         }
         else {
            snailBait.playSound(snailBait.pianoSound);
         }
      },

      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
        var otherSprite;

        if ( ! snailBait.playing) {
            return;
         }

        for(var i=0; i < snailBait.sprites.length; ++i)
        {
          otherSprite = snailBait.sprites[i];
          if(this.isCandidateForCollision(sprite, otherSprite))
          {
            if(this.didCollide(sprite, otherSprite, context))
            {
              this.processCollision(sprite, otherSprite);
            }
          }
        }
      }
    };

    // Bad guy explosions................................................

   this.badGuyExplodeBehaviour = new CellSwitchBehaviour(
      this.explosionCells,
      this.BAD_GUYS_EXPLOSION_DURATION,

      function (sprite, now, fps) { // Trigger
         return sprite.exploding;
      },
                                                
      function (sprite, animator) { // Callback
         sprite.exploding = false;
      }
   );

    //Fall Behaviour
    this.fallBehaviour = {

      pause: function(sprite, now)
      {
        sprite.fallTimer.pause(now);
      },

      unpause: function(sprite, now)
      {
        sprite.fallTimer.unpause(now);
      },

      isOutOfPlay: function(sprite)
      {
        return sprite.top > snailBait.canvas.height;
      },

      setSpriteVelocity: function(sprite, now)
      {
        sprite.velocityY = sprite.initialVelocityY + snailBait.GRAVITY_FORCE*(sprite.fallTimer.getElapsedTime(now)/1000)*snailBait.PIXELS_PER_METRE;
      },

      calculateVerticalDrop: function(sprite, now, lastAnimationFrameTime)
      {
        return sprite.velocityY * (now-lastAnimationFrameTime)/1000;
      },

      willFallBelowCurrentTrack: function(sprite, dropDistance)
      {
        return sprite.top+sprite.height+dropDistance > snailBait.calculatePlatformTop(sprite.track);
      },

      fallOnPlatform: function(sprite)
      {
        sprite.stopFalling();
        snailBait.putSpriteOnTrack(sprite, sprite.track);
        snailBait.playSound(snailBait.thudSound);
      },

      moveDown: function(sprite, now, lastAnimationFrameTime)
      {
        var dropDistance;
        this.setSpriteVelocity(sprite, now);
        dropDistance = this.calculateVerticalDrop(sprite, now, lastAnimationFrameTime);
  
        if(!this.willFallBelowCurrentTrack(sprite, dropDistance))
        {
          sprite.top += dropDistance;
        }
        else
        {
          if(snailBait.platformUnderneath(sprite))
          {
            this.fallOnPlatform(sprite);
            sprite.stopFalling();
          }
          else
          {
            sprite.track--;
            sprite.top += dropDistance;
          }
        }
      },

      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
        if (sprite.falling) {
            if (! this.isOutOfPlay(sprite) && !sprite.exploding) {
               this.moveDown(sprite, now, lastAnimationFrameTime);
            }
            else { // Out of play or exploding               
               sprite.stopFalling();

               if (this.isOutOfPlay(sprite)) {
                  snailBait.loseLife();
                  snailBait.playSound(snailBait.electricityFlowingSound);
                  snailBait.runner.visible = false;
               }
            }
         }
         else { // Not falling
            if ( ! sprite.jumping && 
                 ! snailBait.platformUnderneath(sprite)) {
               sprite.fall();
            }
         }
      }

    };

    //Runner explosions

    this.runnerExplodeBehaviour = new CellSwitchBehaviour(
      this.explosionCells, this.RUNNER_EXPLOSION_DURATION, function (sprite, now, fps, lastAnimationFrameTime){return sprite.exploding;}, function(sprite, animator){ sprite.exploding = false;});

    //Detonate buttons
    this.blueButtonDetonateBehaviour = {
      execute: function(sprite, now, fps, lastAnimationFrameTime)
      {
        var BUTTON_REBOUND_DELAY = 10000,
            SECOND_BEE_EXPLOSION_DELAY = 700;
        if(!sprite.detonating)
        {
          return;
        }
        sprite.artist.cellIndex = 1;
        snailBait.explode(snailBait.bees[5]);
        setTimeout(function(){
          snailBait.explode(snailBait.bees[6]);
          snailBait.setTimeRate(0.1);
        }, SECOND_BEE_EXPLOSION_DELAY);
        sprite.detonating = false;
        setTimeout(function(){
          sprite.artist.cellIndex = 0;
        }, BUTTON_REBOUND_DELAY);

      }
    };

	//Sprites
	//This costs more memory but is faster to iterate through all of the sprites of a particular type
	this.sprites = [];
	this.bats = [];
	this.bees = [];
	this.buttons = [];
	this.coins = [];
	this.platforms = [];
  this.rubies = [];
 	this.sapphires = [];
 	this.snails = [];

	this.platformArtist = 
	{
		draw: function(sprite, context)
		{
			var PLATFORM_STROKE_WIDTH = 1.0,
				PLATFORM_STROKE_STYLE = 'black',
				top;
			top = snailBait.calculatePlatformTop(sprite.track);
			context.lineWidth = PLATFORM_STROKE_WIDTH;
			context.strokeStyle = PLATFORM_STROKE_STYLE;
			context.fillStyle = sprite.fillStyle;

			context.strokeRect(sprite.left, top, sprite.width,
			sprite.height);
			context.fillRect(sprite.left, top, sprite.width,
			sprite.height);

		}
	}
}; //End of constructor

SnailBait.prototype = 
{
	createSprites: function()
	{
		this.createPlatformSprites();
		this.createBatSprites();
		this.createBeeSprites();
		this.createButtonSprites();
		this.createCoinSprites();
		this.createRunnerSprite	();
		this.createRubySprites();
		this.createSapphireSprites();
		this.createSnailSprites();
    //Add all of the sprites to a single array
    this.addSpritesToSpriteArray();
		this.initializeSprites();
	},

  explode: function (sprite) {
      if ( ! sprite.exploding) {
         if (sprite.runAnimationRate === 0) {
            sprite.runAnimationRate = this.RUN_ANIMATION_RATE;
         }
         sprite.exploding = true;
         this.playSound(this.explosionSound);
      }
   },

	addSpritesToSpriteArray: function()
	{
		for(var i =0; i < this.platforms.length; ++i)
		{
			this.sprites.push(this.platforms[i]);
		}
		for(var i =0; i < this.bats.length; ++i)
		{
			this.sprites.push(this.bats[i]);
		}
		for(var i =0; i < this.bees.length; ++i)
		{
			this.sprites.push(this.bees[i]);
		}
		for(var i =0; i < this.buttons.length; ++i)
		{
			this.sprites.push(this.buttons[i]);
		}
		for(var i =0; i < this.coins.length; ++i)
		{
			this.sprites.push(this.coins[i]);
		}
		for(var i =0; i < this.rubies.length; ++i)
		{
			this.sprites.push(this.rubies[i]);
		}
		for(var i =0; i < this.sapphires.length; ++i)
		{
			this.sprites.push(this.sapphires[i]);
		}
		for(var i =0; i < this.snails.length; ++i)
		{
			this.sprites.push(this.snails[i]);
		}

		this.sprites.push(this.runner);
	},

	initializeSprites: function()
	{
		this.positionSprites(this.bats, this.batData);
		this.positionSprites(this.bees, this.beeData);
		this.positionSprites(this.buttons, this.buttonData);
		this.positionSprites(this.coins, this.coinData);
		this.positionSprites(this.rubies, this.rubyData);
		this.positionSprites(this.sapphires, this.sapphireData);
		this.positionSprites(this.snails, this.snailData);

    this.equipRunner();
    this.armSnails();
	},

  equipRunner: function()
  {
    this.equipRunnerForJumping();
    this.equipRunnerForFalling();
    this.runner.direction = snailBait.LEFT;
  },

  equipRunnerForJumping: function()
  {
    var INITIAL_TRACK = 1,
        RUNNER_JUMP_HEIGHT = 120,
        RUNNER_JUMP_DURATION = 1000,
        EASING_FACTOR = 1.1;

    this.runner.JUMP_HEIGHT = RUNNER_JUMP_HEIGHT;
    this.runner.JUMP_DURATION = RUNNER_JUMP_DURATION;
    this.runner.jumping = false;
    this.runner.track = INITIAL_TRACK;
    this.runner.ascendTimer = new AnimationTimer(this.runner.JUMP_DURATION/2, AnimationTimer.makeEaseOutEasingFunction(EASING_FACTOR));
    this.runner.descendTimer = new AnimationTimer(this.runner.JUMP_DURATION/2, AnimationTimer.makeEaseInEasingFunction(EASING_FACTOR));

    this.runner.jump = function()
    {
      if(this.jumping)
      {
        return;
      }
      this.jumping = true;
      this.runAnimationRate = 0;
      this.verticalLaunchPosition = this.top;
      this.ascendTimer.start(snailBait.timeSystem.calculateGameTime());
    };

    this.runner.stopJumping = function()
    {
      this.ascendTimer.stop(snailBait.timeSystem.calculateGameTime());
         this.descendTimer.stop(snailBait.timeSystem.calculateGameTime());
         this.runAnimationRate = snailBait.RUN_ANIMATION_RATE;
         this.jumping = false;
    }; 
  },

  equipRunnerForFalling: function()
  {
    this.runner.fallTimer = new AnimationTimer();
    this.runner.falling = false;
    this.runner.fall = function(initialVelocity)
    {
      this.falling = true;
      this.velocityY = initialVelocity || 0;
      this.initialVelocityY = initialVelocity || 0;
      this.fallTimer.start(snailBait.timeSystem.calculateGameTime());
    };
    this.runner.stopFalling = function()
    {
      this.falling = false;
      this.velocityY = 0;
      this.fallTimer.stop(snailBait.timeSystem.calculateGameTime());
    };
  },

  armSnails: function()
  {
    var snail,
        snailBombArtist = new SpriteSheetArtist(this.spritesheet, this.snailBombCells);

    for(var i=0; i<this.snails.length; ++i)
    {
      snail = this.snails[i];
      snail.bomb = new Sprite('snail bomb', snailBombArtist, [this.snailBombMoveBehaviour]);
      snail.bomb.width = snailBait.SNAIL_BOMB_CELLS_WIDTH;
      snail.bomb.height = snailBait.SNAIL_BOMB_CELLS_HEIGHT;
      snail.bomb.top = snail.top + snail.bomb.height/2;
      snail.bomb.left = snail.left + snail.bomb.width/2;
      snail.bomb.visible = false;
      //Snail bombs maintain reference to their snail
      snail.bomb.snail = snail;
      this.sprites.push(snail.bomb);
    }
  },

	positionSprites: function(sprites, spriteData)
	{
		var sprite;
		for(var i=0; i<sprites.length; ++i)
		{
			sprite = sprites[i];
			if(spriteData[i].platformIndex)
			{
				this.putSpriteOnPlatform(sprite, this.platforms[spriteData[i].platformIndex]);
			}
			else
			{
				sprite.top = spriteData[i].top;
				sprite.left = spriteData[i].left;
			}
		}
	},

	putSpriteOnPlatform: function(sprite, platformSprite)
	{
		sprite.top  = platformSprite.top - sprite.height;
    sprite.left = platformSprite.left;
    sprite.platform = platformSprite;
	},

  putSpriteOnTrack: function(sprite, track)
  {
    sprite.track = track;
    sprite.top = this.calculatePlatformTop(sprite.track) - sprite.height;
  },

  platformUnderneath: function(sprite, track)
  {
    var platform, platformUnderneath,
        sr = sprite.calculateCollisionRectangle(),
        pr;
    if(track === undefined)
    {
      track = sprite.track;
    }
    for(var i=0; i<snailBait.platforms.length; ++i)
    {
      platform = snailBait.platforms[i];
      pr = platform.calculateCollisionRectangle();
      if(track === platform.track)
      {
        if(sr.right > pr.left && sr.left < pr.right)
        {
          platformUnderneath = platform;
          break;
        }
      }
    }
    return platformUnderneath;
  },

	createBatSprites: function()
	{
		var bat,
        BAT_FLAP_DURATION = 120,
        BAT_FLAP_INTERVAL = 100;
		for(var i=0; i<this.batData.length;++i)
		{
			bat = new Sprite('bat', new SpriteSheetArtist(this.spritesheet, this.batCells),[new CycleBehaviour(BAT_FLAP_DURATION, BAT_FLAP_INTERVAL)]);
			//bat cells width varies batCells[1] widest
			bat.width = this.batCells[1].width;
			bat.height = this.BAT_CELLS_HEIGHT;

      bat.collisionMargin = {
            left: 6, top: 11, right: 4, bottom: 8,
         };

			this.bats.push(bat);
		}
	},

	createBeeSprites: function()
	{
		var bee,
        BEE_FLAP_DURATION = 80,
        BEE_FLAP_INTERVAL = 100,
        explodeBehaviour;

    explodeBehaviour = new CellSwitchBehaviour(
         this.explosionCells,
         this.BAD_GUYS_EXPLOSION_DURATION,

         function (sprite, now, fps, lastAnimationFrameTime) { // Trigger
            return sprite.exploding;
         },
                                                
         function (sprite, animator) { // Callback
            sprite.exploding = false;
         }
      ); 

		for(var i=0; i<this.beeData.length;++i)
		{
			bee = new Sprite('bee', new SpriteSheetArtist(this.spritesheet, this.beeCells), [new CycleBehaviour(BEE_FLAP_DURATION, BEE_FLAP_INTERVAL), explodeBehaviour]);
			bee.width = this.BEE_CELLS_WIDTH;
			bee.height = this.BEE_CELLS_HEIGHT;
      bee.collisionMargin = {
            left: 10, top: 10, right: 5, bottom: 10,
      };
			this.bees.push(bee);
		}
	},

	createButtonSprites: function()
	{
		var button;
		for(var i=0; i<this.buttonData.length;++i)
		{
			if(i !== this.buttonData.length-1)
			{
				button = new Sprite('button', new SpriteSheetArtist(this.spritesheet, this.blueButtonCells));
        button.behaviours = [this.paceBehaviour, this.blueButtonDetonateBehaviour];
			}
			else
			{
				button = new Sprite('button', new SpriteSheetArtist(this.spritesheet, this.goldButtonCells));
			}


			button.width = this.BUTTON_CELLS_WIDTH;
			button.height = this.BUTTON_CELLS_HEIGHT;
			button.velocityX = this.BUTTON_PACE_VELOCITY;
			this.buttons.push(button);
		}
	},

	createCoinSprites: function()
	{
		var coin,
        COIN_SPARKLE_DURATION = 300,
        COIN_SPARKLE_INTERVAL = 600,
        BOUNCE_DURATION_BASE = 800,
        BOUNCE_HEIGHT_BASE = 50;
		for(var i=0; i<this.coinData.length;++i)
		{
			if(i%2 === 0)
			{
				coin = new Sprite('coin', new SpriteSheetArtist(this.spritesheet, this.goldCoinCells), [new CycleBehaviour(COIN_SPARKLE_DURATION, COIN_SPARKLE_INTERVAL), new BounceBehaviour(BOUNCE_DURATION_BASE + BOUNCE_DURATION_BASE*Math.random(), BOUNCE_HEIGHT_BASE + BOUNCE_HEIGHT_BASE*Math.random())]);
			}
			else
			{
				coin = new Sprite('coin', new SpriteSheetArtist(this.spritesheet, this.blueCoinCells), [new CycleBehaviour(COIN_SPARKLE_DURATION, COIN_SPARKLE_INTERVAL), new BounceBehaviour(BOUNCE_DURATION_BASE + BOUNCE_DURATION_BASE*Math.random(), BOUNCE_HEIGHT_BASE + BOUNCE_HEIGHT_BASE*Math.random())]);
			}
			coin.width = this.COIN_CELLS_WIDTH;
			coin.height = this.COIN_CELLS_HEIGHT;
			coin.value = 50;

      coin.collisionMargin = {
            left:   coin.width/8, top:    coin.height/8,
            right:  coin.width/8, bottom: coin.height/4
         };
			this.coins.push(coin);
		}
	},

	createPlatformSprites: function()
	{
		var sprite, pd,
        PULSE_DURATION = 800,
        PULSE_OPACITY_THRESHOLD = 0.1;
		for(var i=0; i<this.platformData.length;++i)
		{
			pd = this.platformData[i];
			sprite = new Sprite('platform', this.platformArtist);
			sprite.left = pd.left;
			sprite.width = pd.width;
			sprite.height = pd.height;
			sprite.fillStyle = pd.fillStyle;
			sprite.opacity = pd.opacity;
			sprite.track = pd.track;
			sprite.button = pd.button;
			sprite.pulsate = pd.pulsate;

			sprite.top = this.calculatePlatformTop(pd.track);
      if(sprite.pulsate)
      {
        sprite.behaviours = [new PulseBehaviour(PULSE_DURATION, PULSE_OPACITY_THRESHOLD)];
      }

			this.platforms.push(sprite);
		}
	},

	createRubySprites: function()
	{
		var SPARKLE_DURATION = 100,
          BOUNCE_DURATION_BASE = 1000, // milliseconds
          BOUNCE_HEIGHT_BASE = 100,   // pixels
          ruby,
          rubyArtist = new SpriteSheetArtist(this.spritesheet,
                                             this.rubyCells);
   
      for (var i = 0; i < this.rubyData.length; ++i) {
         ruby = new Sprite('ruby', 
                            rubyArtist,
                            [ new CycleBehaviour(SPARKLE_DURATION),

                              new BounceBehaviour(BOUNCE_DURATION_BASE +
                                    BOUNCE_DURATION_BASE * Math.random(),

                                    BOUNCE_HEIGHT_BASE +
                                    BOUNCE_HEIGHT_BASE * Math.random()) ]);

         ruby.width = this.RUBY_CELLS_WIDTH;
         ruby.height = this.RUBY_CELLS_HEIGHT;
         ruby.value = 200;

         ruby.collisionMargin = {
            left: ruby.width/5,
            top: ruby.height/8,
            right: 0,
            bottom: ruby.height/4
         };
         
         this.rubies.push(ruby);
      }
	},

	createSapphireSprites: function()
	{
		var SPARKLE_DURATION = 100,
          BOUNCE_DURATION_BASE = 3000, // milliseconds
          BOUNCE_HEIGHT_BASE = 100,          // pixels
          sapphire,
          sapphireArtist = new SpriteSheetArtist(this.spritesheet,
                                                 this.sapphireCells);
   
      for (var i = 0; i < this.sapphireData.length; ++i) {
         sapphire = new Sprite('sapphire', 
                               sapphireArtist,
                               [ new CycleBehaviour(SPARKLE_DURATION),

                                 new BounceBehaviour(BOUNCE_DURATION_BASE +
                                    BOUNCE_DURATION_BASE * Math.random(),

                                    BOUNCE_HEIGHT_BASE +
                                    BOUNCE_HEIGHT_BASE * Math.random()) ]);

         sapphire.width = this.SAPPHIRE_CELLS_WIDTH;
         sapphire.height = this.SAPPHIRE_CELLS_HEIGHT;
         sapphire.value = 100;

         sapphire.collisionMargin = {
            left:   sapphire.width/8, top:    sapphire.height/8,
            right:  sapphire.width/8, bottom: sapphire.height/4
         }; 

         this.sapphires.push(sapphire);
      }
	},

	createRunnerSprite: function()
	{
		var RUNNER_LEFT = 50,
		    RUNNER_HEIGHT = 53,
        STARTING_RUN_ANIMATION_RATE = 0,
		    STARTING_RUNNER_TRACK = 1;

		this.runner = new Sprite('runner', new SpriteSheetArtist(this.spritesheet, this.runnerCellsRight), [this.runBehaviour, this.jumpBehaviour, this.collideBehaviour, this.fallBehaviour, this.runnerExplodeBehaviour]);
    this.runner.runAnimationRate = STARTING_RUN_ANIMATION_RATE;
		this.runner.track = STARTING_RUNNER_TRACK;
		this.runner.left = RUNNER_LEFT;
		
    this.runner.width = this.RUNNER_CELLS_WIDTH;
    this.runner.height = this.RUNNER_CELLS_HEIGHT;
    this.putSpriteOnTrack(this.runner, STARTING_RUNNER_TRACK);
    this.runner.collisionMargin = {
          left: 15,
          top: 10, 
          right: 10,
          bottom: 10,
       };
		this.sprites.push(this.runner);
	},

  putSpriteOnTrack: function(sprite, track) {
      var SPACE_BETWEEN_SPRITE_AND_TRACK = 2;

      sprite.track = track;
      sprite.top = this.calculatePlatformTop(sprite.track) - 
                   sprite.height - SPACE_BETWEEN_SPRITE_AND_TRACK;
   },

	createSnailSprites: function()
	{
		var snail,
        SNAIL_CYCLE_DURATION = 300,
        SNAIL_CYCLE_INTERVAL = 3500;
		for(var i=0; i<this.snailData.length;++i)
		{
			snail = new Sprite('snail', new SpriteSheetArtist(this.spritesheet, this.snailCells), [this.paceBehaviour, this.snailShootBehaviour, new CycleBehaviour(SNAIL_CYCLE_DURATION, SNAIL_CYCLE_INTERVAL)]);
			snail.width = this.SNAIL_CELLS_WIDTH;
			snail.height = this.SNAIL_CELLS_HEIGHT;
			snail.velocityX = snailBait.SNAIL_PACE_VELOCITY;
			this.snails.push(snail);
		}
	},


	//Animation
	animate: function(now)
	{
    //Replace the time passed to animate by the browser
    //with our own game time
    now = snailBait.timeSystem.calculateGameTime();
		if(snailBait.paused)
		{
			setTimeout(function(){
				requestAnimationFrame(snailBait.animate);
			}, snailBait.PAUSED_CHECK_INTERVAL);
		}
		else
		{
			snailBait.fps = snailBait.calculateFps(now);
			snailBait.draw(now);
			snailBait.lastAnimationFrameTime = now;
			requestAnimationFrame(snailBait.animate);
		}
	},

	calculateFps: function(now)
	{
		var fps = 1/(now - snailBait.lastAnimationFrameTime) * 1000*
    this.timeRate;
		if(now - snailBait.lastFpsUpdateTime > 1000)
		{
			snailBait.lastFpsUpdateTime = now;
			snailBait.fpsElement.innerHTML = fps.toFixed(0) + ' fps'; 
		}
		return fps;
	},

	initializeImages: function()
	{
		snailBait.loadingAnimatedGIFElement.src = 'images/snail.gif';
		this.spritesheet.src = 'images/spritesheet.png';

		this.spritesheet.onload = function(e)
		{
			snailBait.spritesheetLoaded();
		};

		this.loadingAnimatedGIFElement.onload = function(e)
		{
			snailBait.loadingAnimationLoaded();
		};
	},

	backgroundLoaded: function()
	{
		var LOADING_SCREEN_TRANSITION_DURATION = 2000;
		this.fadeOutElements(this.loadingElement, LOADING_SCREEN_TRANSITION_DURATION);
		setTimeout(function(e){
			snailBait.startGame();
			snailBait.gameStarted = true;
		}, LOADING_SCREEN_TRANSITION_DURATION);
	},

  spritesheetLoaded: function () {
      var LOADING_SCREEN_TRANSITION_DURATION = 2000;

      this.graphicsReady = true;

      this.fadeOutElements(this.loadingElement, 
         LOADING_SCREEN_TRANSITION_DURATION);

      setTimeout ( function () {
         if (! snailBait.gameStarted) {
            snailBait.startGame();
         }
      }, LOADING_SCREEN_TRANSITION_DURATION);
   },

	loadingAnimationLoaded: function()
	{
		if(!this.gameStarted)
		{
			this.fadeInElements(this.loadingAnimatedGIFElement, this.loadingTitleElement);
		}
	},

	startGame: function()
	{
		//this.togglePaused();
		this.revealGame();
    if(snailBait.mobile)
    {
      this.fadeInElements(snailBait.mobileWelcomeToast);
    }
    else
    {
		  this.revealInitialToast();
      this.playing = true;
    }
    this.startMusic();
    this.timeSystem.start();
    //this.setTimeRate(0.5);
    this.gameStarted = true;
		window.requestAnimationFrame(snailBait.animate);
	},

  setTimeRate: function(rate)
  {
    this.timeRate = rate;
    this.timeSystem.setTransducer(function(percent)
    {
      return percent * snailBait.timeRate;
    });
  },

	revealGame: function()
	{
		var DIM_CONTROLS_DELAY = 5000;
		this.revealTopChromeDimmed();
		this.revealCanvas();
		this.revealBottomChrome();

		setTimeout(function() {
			snailBait.dimControls();
			snailBait.revealTopChrome();
		}, DIM_CONTROLS_DELAY);
	},

	revealTopChromeDimmed: function()
	{
		var DIM = 0.25;
		this.scoreElement.style.display = 'block';
		this.fpsElement.style.display = 'block';
		setTimeout(function(){
			snailBait.scoreElement.style.opacity = 'block';
			snailBait.fpsElement.style.opacity = 'block';
		}, this.SHORT_DELAY);
	},

	revealCanvas: function()
	{
		this.fadeInElements(this.canvas);
	},

	revealBottomChrome: function()
	{
		this.fadeInElements(this.soundAndMusicElement, this.instructionElement, this.copyrightElement);
	},

	dimControls: function()
	{
		var FINAL_OPACITY = 0.5;
		snailBait.instructionElement.style.opacity = FINAL_OPACITY;
		snailBait.soundAndMusicElement.style.opacity = FINAL_OPACITY;
	},

	revealTopChrome: function()
	{
		this.fadeInElements(this.fpsElement, this.scoreElement);
	},

	revealInitialToast: function()
	{
		var INITIAL_TOAST_DELAY = 1500,
		    INITIAL_TOAST_DURATION = 3000;
		setTimeout(function(){
			snailBait.revealToast('Collide with coins and jewels. Avoid bees and bats', INITIAL_TOAST_DURATION);
		}, INITIAL_TOAST_DELAY);
	},



	draw: function(now)
	{
		snailBait.setPlatformVelocity();
		snailBait.setOffsets(now);
		snailBait.drawBackground();
		this.updateSprites(now);
		//console.log("About to call drawSprites");
		this.drawSprites();

    if (this.mobileInstructionsVisible) {
         snailBait.drawMobileInstructions();
      }
		//snailBait.drawRunner();
		//snailBait.drawPlatforms();
	},

	updateSprites: function(now)
	{
		var sprite;
		for(var i=0; i<this.sprites.length; ++i)
		{
			sprite = this.sprites[i];
			if(sprite.visible && this.isSpriteInView(sprite))
			{
				//this.context.translate(-sprite.hOffset, 0);
				sprite.update(now, this.fps, this.context, this.lastAnimationFrameTime);
				//this.context.translate(sprite.hOffset, 0);
			}
		}
	},

	drawSprites: function()
	{
		var sprite;
		for(var i=0; i<this.sprites.length; ++i)
		{
			//console.log(this.sprite[i].type);
			sprite = this.sprites[i];
			if(sprite.visible && this.isSpriteInView(sprite))
			{
				this.context.translate(-sprite.hOffset, 0);
				sprite.draw(this.context);
				this.context.translate(sprite.hOffset, 0);
			}
		}
	},

	isSpriteInView: function(sprite)
	{
		return sprite.left+sprite.width > sprite.hOffset && sprite.left < sprite.hOffset + this.canvas.width;
	},

	drawBackground: function()
	{
		var BACKGROUND_TOP_IN_SPRITESHEET = 590;

      // Translate everything by the background offset
      this.context.translate(-this.backgroundOffset, 0);

      // 2/3 onscreen initially:
      this.context.drawImage(
         this.spritesheet, 0, BACKGROUND_TOP_IN_SPRITESHEET, 
         this.BACKGROUND_WIDTH, this.BACKGROUND_HEIGHT,
         0, 0,
         this.BACKGROUND_WIDTH, this.BACKGROUND_HEIGHT);

      // Initially offscreen:
      this.context.drawImage(
         this.spritesheet, 0, BACKGROUND_TOP_IN_SPRITESHEET, 
         this.BACKGROUND_WIDTH, this.BACKGROUND_HEIGHT,
         this.BACKGROUND_WIDTH, 0,
         this.BACKGROUND_WIDTH, this.BACKGROUND_HEIGHT);

      // Translate back to the original location
      this.context.translate(this.backgroundOffset, 0);
	},

	drawRunner: function()
	{
		snailBait.context.drawImage(snailBait.runner, snailBait.RUNNER_LEFT, snailBait.calculatePlatformTop(snailBait.STARTING_RUNNER_TRACK) - snailBait.runner.height);
	},

	drawPlatform: function(data)
	{
		var platformTop = snailBait.
		calculatePlatformTop(data.track);
		snailBait.context.lineWidth = snailBait.PLATFORM_STROKE_WIDTH;
		snailBait.context.strokeStyle = snailBait.PLATFORM_STROKE_STYLE;
		snailBait.context.fillStyle = data.fillStyle;
		snailBait.context.globalAlpha = data.opacity;
		snailBait.context.strokeRect(data.left, platformTop, data.width, data.height);
		snailBait.context.fillRect(data.left, platformTop, data.width, data.height);
	},

	drawPlatforms: function()
	{
		var index;
		snailBait.context.translate(-snailBait.platformOffset, 0);
		for(index = 0; index < snailBait.platformData.length; ++index)
		{
			snailBait.drawPlatform(snailBait.platformData[index]);
		}
		snailBait.context.translate(snailBait.platformOffset, 0);
	},

	calculatePlatformTop: function(track)
	{
		if(track === 1)
		{
			return snailBait.TRACK_1_BASELINE;
		}
		else if(track === 2)
		{
			return snailBait.TRACK_2_BASELINE;
		}
		else if(track === 3)
		{
			return snailBait.TRACK_3_BASELINE;
		}
	},

	setBackgroundOffset: function(now)
	{
		snailBait.backgroundOffset += snailBait.bgVelocity*(now - snailBait.lastAnimationFrameTime)/1000;
		if(snailBait.backgroundOffset < 0 || snailBait.backgroundOffset > snailBait.BACKGROUND_WIDTH)
		{
			snailBait.backgroundOffset = 0;
		}
	},

	turnLeft: function()
	{
		snailBait.bgVelocity = -snailBait.BACKGROUND_VELOCITY;
		this.runner.runAnimationRate = this.RUN_ANIMATION_RATE;
		this.runner.artist.cells = this.runnerCellsLeft;
    this.runner.direction = snailBait.LEFT;
	},

	turnRight: function()
	{
		snailBait.bgVelocity = snailBait.BACKGROUND_VELOCITY;
		this.runner.runAnimationRate = this.RUN_ANIMATION_RATE;
		this.runner.artist.cells = this.runnerCellsRight;
    this.runner.direction = snailBait.RIGHT;
	},

	setPlatformVelocity: function()
	{
		snailBait.platformVelocity = snailBait.bgVelocity * snailBait.PLATFORM_VELOCITY_MULTIPLER;
	},

	setPlatformOffset: function(now)
	{
		snailBait.platformOffset += snailBait.platformVelocity*(now - snailBait.lastAnimationFrameTime)/1000;
		//console.log(snailBait.platformOffset);
		if(snailBait.platformOffset > 2*snailBait.background.width)
		{
			snailBait.turnLeft();
		}
		else if(snailBait.platformOffset < 0)
		{
			snailBait.turnRight();
		}
	},

	setOffsets: function(now)
	{
		snailBait.setBackgroundOffset(now);
		snailBait.setSpriteOffsets(now);
		//snailBait.setPlatformOffset(now);
	},

	setSpriteOffsets: function (now) {
      var sprite;
   
      // In step with platforms
      this.spriteOffset +=
         this.platformVelocity * (now - this.lastAnimationFrameTime) / 1000;

      for (var i=0; i < this.sprites.length; ++i) {
         sprite = this.sprites[i];

         if ('runner' !== sprite.type) {
            sprite.hOffset = this.spriteOffset; 
         }
      }
   },

	togglePaused: function()
	{
		var now = this.timeSystem.calculateGameTime();
		this.paused = !this.paused;
    //this.togglePausedStateOfAllBehaviours(now);
		if(this.paused)
		{
			this.pauseStartTime = now;
      this.revealToast('Paused');
		}
		else
		{
			this.lastAnimationFrameTime += (now - this.pauseStartTime);
		}
    if(this.musicOn)
    {
      if(this.paused)
      {
        this.musicElement.pause();
      }
      else
      {
        this.musicElement.play();
      }
    }
	},

  togglePausedStateOfAllBehaviours: function (now) {
      var behaviour;
   
      for (var i=0; i < this.sprites.length; ++i) {
         sprite = this.sprites[i];

         for (var j=0; j < sprite.behaviours.length; ++j) {
            behaviour = sprite.behaviours[j];

            if (this.paused) {
               if (behaviour.pause) {
                  behaviour.pause(sprite, now);
               }
            }
            else {
               if (behaviour.unpause) {
                  behaviour.unpause(sprite, now);
               }
            }
         }
      }
   },

	revealToast: function(text, duration)
	{
		var DEFAULT_TOAST_DURATION = 1000;
		duration = duration || DEFAULT_TOAST_DURATION;
		this.startToastTransition(text);
		setTimeout(function(e){
			snailBait.hideToast();
		}, duration);
	},

	startToastTransition: function(text)
	{
		this.toastElement.innerHTML = text;
		this.fadeInElements(this.toastElement);
	},

	hideToast: function()
	{
		var TOAST_TRANSITION_DURATION = 500;
		this.fadeOutElements(this.toastElement, TOAST_TRANSITION_DURATION);
	},

	fadeInElements: function()
	{
		var args = arguments;
		for(var i=0; i < args.length; ++i)
		{
			args[i].style.display = 'block';
		}
		setTimeout(function(e){
			for(var i=0; i < args.length; ++i)
			{
				args[i].style.opacity = snailBait.OPAQUE;
			}
		}, this.SHORT_DELAY);
	},

	fadeOutElements: function()
	{
		var args = arguments,
		    fadeDuration = args[args.length-1];

		for(var i=0; i < args.length-1; ++i)
		{
			args[i].style.opacity = snailBait.TRANSPARENT;
		}
		setTimeout(function(e){
			for(var i=0; i < args.length-1; ++i)
			{
				args[i].style.display = 'none';
			}
		}, fadeDuration);
	},

  shake: function () {
      var SHAKE_INTERVAL = 80, // milliseconds
          v = snailBait.BACKGROUND_VELOCITY*1.5,
          ov = snailBait.bgVelocity; // ov means original velocity
   
      snailBait.bgVelocity = -v;

      setTimeout( function (e) {
       snailBait.bgVelocity = v;
       setTimeout( function (e) {
          snailBait.bgVelocity = -v;
          setTimeout( function (e) {
             snailBait.bgVelocity = v;
             setTimeout( function (e) {
                snailBait.bgVelocity = -v;
                setTimeout( function (e) {
                   snailBait.bgVelocity = v;
                   setTimeout( function (e) {
                      snailBait.bgVelocity = -v;
                      setTimeout( function (e) {
                         snailBait.bgVelocity = v;
                         setTimeout( function (e) {
                            snailBait.bgVelocity = -v;
                            setTimeout( function (e) {
                               snailBait.bgVelocity = v;
                               setTimeout( function (e) {
                                  snailBait.bgVelocity = -v;
                                  setTimeout( function (e) {
                                     snailBait.bgVelocity = v;
                                     setTimeout( function (e) {
                                        snailBait.bgVelocity = ov;
                                     }, SHAKE_INTERVAL);
                                  }, SHAKE_INTERVAL);
                               }, SHAKE_INTERVAL);
                            }, SHAKE_INTERVAL);
                         }, SHAKE_INTERVAL);
                      }, SHAKE_INTERVAL);
                   }, SHAKE_INTERVAL);
                }, SHAKE_INTERVAL);
             }, SHAKE_INTERVAL);
          }, SHAKE_INTERVAL);
       }, SHAKE_INTERVAL);
     }, SHAKE_INTERVAL);
   },

   loseLife: function()
   {
    var TRANSITION_DURATION = 3000;
    this.lives--;
    this.startLifeTransition(snailBait.RUNNER_EXPOSION_DURATION);
    setTimeout(function(){
      snailBait.endLifeTransition();
    }, TRANSITION_DURATION);
   },

   startLifeTransition: function(delay)
   {
      var CANVAS_TRANSITION_OPACITY = 0.05,
        SLOW_MOTION_RATE = 0.1;
      if (delay === undefined) {
         delay = 0;
      }
      this.canvas.style.opacity = CANVAS_TRANSITION_OPACITY;
      this.playing = false;
      setTimeout(function(){
        snailBait.setTimeRate(SLOW_MOTION_RATE);
        snailBait.runner.visible = false;
      }, delay);
   },

   endLifeTransition: function()
   {
      var TIME_RESET_DELAY = 1000,
          RUN_DELAY = 500;
      snailBait.reset();
      setTimeout(function(){
        snailBait.setTimeRate(1.0);
        setTimeout(function(){
          snailBait.runner.runAnimationRate = 0;
          snailBait.playing = true;
        }, RUN_DELAY);
      }, TIME_RESET_DELAY);
   },

   reset: function()
   {
     snailBait.resetOffsets();
     snailBait.resetRunner();
     snailBait.makeAllSpritesVisible();
     snailBait.canvas.style.opacity = 1.0;
   },

   resetRunner: function()
   {
      this.runner.left = snailBait.RUNNER_LEFT;
      this.runner.track = 3;
      this.runner.hOffset = 0;
      this.runner.visible = true;
      this.runner.exploding = false;
      this.runner.jumping = false;
      this.runner.top = this.calculatePlatformTop(3) - this.runner.height;
      this.runner.artist.cells = this.runnerCellsRight;
      this.runner.artist.cellIndex = 0;
   },

   resetOffsets: function()
   {
      this.bgVelocity = 0;
      this.backgroundOffset = 0;
      this.platformOffset = 0;
      this.spriteOffset = 0;
   },

   makeAllSpritesVisible: function()
   {
      for(var i=0; i < snailBait.sprites.length; ++i)
      {
        snailBait.sprites[i].visible = true;
      }
   },

   //Sounds 
   createAudioChannels: function()
   {
      var channel;
      for(var i=0; i < this.audioChannels.length; ++i)
      {
        channel = this.audioChannels[i];
        if(i !== 0)
        {
          channel.audio = document.createElement('audio');
          channel.audio.addEventListener('loadeddata', this.soundLoaded, false);
          channel.audio.src = this.audioSprites.currentSrc;
        }
        channel.audio.autobuffer = true;
      }
   },

   seekAudio: function(sound, audio)
   {
      try{
        audio.pause();
        audio.currentTime = sound.position;
      }
      catch(e)
      {
        if(console)
        {
          console.error("Cannot seek audio");
        }
      }
   },

   playAudio: function(audio, channel)
   {
      try{
        audio.play();
        channel.playing = true;
      }
      catch(e)
      {
        if(console)
        {
          console.error("Cannot play audio");
        }
      }
   },

   soundLoaded: function()
   {
      snailBait.audioSpriteCountdown--;
      if(snailBait.audioSpriteCountdown === 0)
      {
         if(!snailBait.gameStarted && snailBait.graphicsReady)
         {
            snailBait.startGame();
         }
      }
   },

   getFirstAvailableAudioChannel: function()
   {
      for(var i=0; i < this.audioChannels.length;++i)
      {
          if(!this.audioChannels[i].playing)
          {
            return this.audioChannels[i];
          }
      }
      return null;
   },

   playSound: function(sound)
   {
      var channel, audio;
      if(this.soundOn)
      {
        channel = this.getFirstAvailableAudioChannel();
        if(!channel)
        {
          if(console)
          {
            console.warn('All audio channels are busy. Cannot play the sound');
          }
        }
        else
        {
          audio = channel.audio;
          audio.volume = sound.volume;
          this.seekAudio(sound, audio);
          this.playAudio(audio, channel);

          setTimeout(function() {
            channel.playing = false;
            snailBait.seekAudio(sound, audio);
          }, sound.duration);
        }
      }
   },

   pollMusic: function()
   {
      var POLL_INTERVAL = 500,
          SOUNDTRACK_LENGTH = 132,
          timerID;
      timerID = setInterval(function(){
        if(snailBait.musicElement.currentTime > SOUNDTRACK_LENGTH)
        {
          clearInterval(timerID); //Stop polling
          snailBait.restartMusic();
        }
      }, POLL_INTERVAL);
   },

   restartMusic: function()
   {
      snailBait.musicElement.pause();
      snailBait.musicElement.currentTime = 0;
      snailBait.startMusic();
   },

   startMusic: function()
   {
      var MUSIC_DELAY = 1000;
      setTimeout(function(){
        if(snailBait.musicCheckboxElement.checked)
        {
          snailBait.musicElement.play();
        }
        snailBait.pollMusic();
      }, MUSIC_DELAY);
   },

   getViewportSize: function () {
      return { 
        width: Math.max(document.documentElement.clientWidth ||
               window.innerWidth || 0),  
               
        height: Math.max(document.documentElement.clientHeight ||
                window.innerHeight || 0)
      };
   },

   detectMobile: function () {
      snailBait.mobile = 'ontouchstart' in window;
   },

   resizeElement: function (element, w, h) {
      console.log(element);
      element.style.width  = w + 'px';
      element.style.height = h + 'px';
   },

   resizeElementsToFitScreen: function (arenaWidth, arenaHeight) {
      snailBait.resizeElement(
         document.getElementById('arena'), 
         arenaWidth, arenaHeight);

      snailBait.resizeElement(snailBait.mobileWelcomeToast,
                              arenaWidth, arenaHeight);

      snailBait.resizeElement(snailBait.mobileStartToast,
                              arenaWidth, arenaHeight);
   },

   calculateArenaSize: function (viewportSize) {
      var DESKTOP_ARENA_WIDTH  = 800,  // Pixels
          DESKTOP_ARENA_HEIGHT = 520,  // Pixels
          arenaHeight,
          arenaWidth;

      arenaHeight = viewportSize.width * 
                    (DESKTOP_ARENA_HEIGHT / DESKTOP_ARENA_WIDTH);

      if (arenaHeight < viewportSize.height) { // Height fits
         arenaWidth = viewportSize.width;      // Set width
      }
      else {                                   // Height does not fit
         arenaHeight = viewportSize.height;    // Recalculate height
         arenaWidth  = arenaHeight *           // Set width
                      (DESKTOP_ARENA_WIDTH / DESKTOP_ARENA_HEIGHT);
      }

      if (arenaWidth > DESKTOP_ARENA_WIDTH) {  // Too wide
         arenaWidth = DESKTOP_ARENA_WIDTH;     // Limit width
      } 

      if (arenaHeight > DESKTOP_ARENA_HEIGHT) { // Too tall
         arenaHeight = DESKTOP_ARENA_HEIGHT;    // Limit height
      }

      return { 
         width:  arenaWidth, 
         height: arenaHeight 
      };
   },

   fitScreen: function () {
      var arenaSize = snailBait.calculateArenaSize(
                         snailBait.getViewportSize());

      snailBait.resizeElementsToFitScreen(arenaSize.width, 
                                          arenaSize.height);
   },

   processRightTap: function () {
      if (snailBait.runner.direction === snailBait.LEFT ||
          snailBait.bgVelocity === 0) {
         snailBait.turnRight();
      }
      else {
         snailBait.runner.jump();
      }
   },

   processLeftTap: function () {
      if (snailBait.runner.direction === snailBait.RIGHT) {
         snailBait.turnLeft();
      }
      else {
         snailBait.runner.jump();
      }
   },

   touchStart: function (e) {
      if (snailBait.playing) {
         // Prevent players from inadvertently 
         // dragging the game canvas
         e.preventDefault(); 
      }
   },

   touchEnd: function (e) {
      var x = e.changedTouches[0].pageX;

      if (snailBait.playing) {
         if (x < snailBait.canvas.width/2) {
            snailBait.processLeftTap();
         }
         else if (x > snailBait.canvas.width/2) {
            snailBait.processRightTap();
         }

         // Prevent players from double
         // tapping to zoom into the canvas

         e.preventDefault(); 
      }
   },

   addTouchEventHandlers: function () {
      snailBait.canvas.addEventListener(
         'touchstart', 
         snailBait.touchStart
      );

      snailBait.canvas.addEventListener(
         'touchend', 
         snailBait.touchEnd
      );
   },

   initializeContextForMobileInstructions: function () {
      this.context.textAlign = 'center';
      this.context.textBaseline = 'middle';

      this.context.font = '26px fantasy';

      this.context.shadowBlur = 2;
      this.context.shadowOffsetX = 2;
      this.context.shadowOffsetY = 2;
      this.context.shadowColor = 'rgb(0,0,0)';

      this.context.fillStyle = 'yellow';
      this.context.strokeStyle = 'yellow';
   },

   drawMobileDivider: function (cw, ch) {
      this.context.beginPath();
      this.context.moveTo(cw/2, 0);
      this.context.lineTo(cw/2, ch);
      this.context.stroke();
   },

   drawMobileInstructionsLeft: function (cw, ch, 
                                         topLineOffset, lineHeight) {
      this.context.font = '32px fantasy';

      this.context.fillText('Tap on this side to:', 
         cw/4, ch/2 - topLineOffset);

      this.context.fillStyle = 'white';
      this.context.font = 'italic 26px fantasy';

      this.context.fillText('Turn around when running right', 
         cw/4, ch/2 - topLineOffset + 2*lineHeight);

      this.context.fillText('Jump when running left', 
         cw/4, ch/2 - topLineOffset + 3*lineHeight);
   },

   drawMobileInstructionsRight: function (cw, ch, 
                                          topLineOffset, lineHeight) {
      this.context.font = '32px fantasy';
      this.context.fillStyle = 'yellow';

      this.context.fillText('Tap on this side to:', 
         3*cw/4, ch/2 - topLineOffset);

      this.context.fillStyle = 'white';
      this.context.font = 'italic 26px fantasy';

      this.context.fillText('Turn around when running left', 
         3*cw/4, ch/2 - topLineOffset + 2*lineHeight);

      this.context.fillText('Jump when running right', 
         3*cw/4, ch/2 - topLineOffset + 3*lineHeight);

      this.context.fillText('Start running', 
         3*cw/4, ch/2 - topLineOffset + 5*lineHeight);
   },

   drawMobileInstructions: function () {
      var cw = this.canvas.width,
          ch = this.canvas.height,
          TOP_LINE_OFFSET = 115,
          LINE_HEIGHT = 40;

      this.context.save();

      this.initializeContextForMobileInstructions();

      this.drawMobileDivider(cw, ch);

      this.drawMobileInstructionsLeft(cw, ch, 
                                      TOP_LINE_OFFSET, 
                                      LINE_HEIGHT);

      this.drawMobileInstructionsRight(cw, ch, 
                                       TOP_LINE_OFFSET, 
                                       LINE_HEIGHT);

      this.context.restore();
   },

   revealMobileStartToast: function () {
      snailBait.fadeInElements(snailBait.mobileStartToast);
   },



};

window.addEventListener('keydown', function(e){
	var key = e.keyCode;
  if(!snailBait.playing || snailBait.runner.exploding)
  {
    return;
  }
	if(key === snailBait.KEY_D || key === snailBait.KEY_LEFT) 
	{
		snailBait.turnLeft();
	}
	else if(key === snailBait.KEY_K || key === snailBait.KEY_RIGHT) 
	{
		snailBait.turnRight();
	}
	else if(key === snailBait.KEY_P)
	{
		snailBait.togglePaused();
	}
  else if(key === 74)
  {
    snailBait.runner.jump();
  }
});

window.addEventListener('blur', function(e)
{
	snailBait.windowHasFocus = false;
	if(!snailBait.paused)
	{
		snailBait.togglePaused(); //Pause the game
}});

window.addEventListener('focus', function(e)
{
	var originalFont = snailBait.toastElement.style.fontSize,
	DIGIT_DISPLAY_DURATION = 1000;

	snailBait.windowHasFocus = true;
	snailBait.countDownInProgress = true;
	if(snailBait.paused)
	{
		snailBait.toastElement.style.font = '128px fantasy';
		if(snailBait.windowHasFocus && snailBait.countDownInProgress)
		{
			snailBait.revealToast('3', DIGIT_DISPLAY_DURATION);
		}
		setTimeout(function (e){
			if(snailBait.windowHasFocus && snailBait.countDownInProgress)
			{
				snailBait.revealToast('2', DIGIT_DISPLAY_DURATION);
			}
			setTimeout(function (e){
			if(snailBait.windowHasFocus && snailBait.countDownInProgress)
			{
				snailBait.revealToast('1', DIGIT_DISPLAY_DURATION);
			}
				setTimeout(function (e){
			if(snailBait.windowHasFocus && snailBait.countDownInProgress)
			{
				snailBait.togglePaused();
			}
			if(snailBait.windowHasFocus && snailBait.countDownInProgress)
			{
				snailBait.toastElement.style.fontSize = originalFont;
			}
			snailBait.countdownInProgress = false;
			}, DIGIT_DISPLAY_DURATION);
			}, DIGIT_DISPLAY_DURATION);
		},DIGIT_DISPLAY_DURATION);
	}
});


var snailBait = new SnailBait();

snailBait.showHowLink.addEventListener(
   'click',

   function (e) {
      var FADE_DURATION = 1000;

      snailBait.fadeOutElements(snailBait.mobileWelcomeToast, 
                                FADE_DURATION);

      snailBait.drawMobileInstructions();
      snailBait.revealMobileStartToast();
      snailBait.mobileInstructionsVisible = true;
   }
);

snailBait.mobileStartLink.addEventListener(
   'click', 

   function (e) {
      var FADE_DURATION = 1000;

      snailBait.fadeOutElements(snailBait.mobileStartToast, 
                                FADE_DURATION);

      snailBait.mobileInstructionsVisible = false;
      snailBait.playSound(snailBait.coinSound);
      snailBait.playing = true;
   }
);

snailBait.welcomeStartLink.addEventListener(
   'click',

   function (e) {
      var FADE_DURATION = 1000;

      snailBait.playSound(snailBait.coinSound);
      snailBait.fadeOutElements(snailBait.mobileWelcomeToast, 
                                FADE_DURATION);
      snailBait.playing = true;
   }
);

//Sound and music checkbox event handlers
snailBait.musicCheckboxElement.addEventListener('change',
  function(e){
    snailBait.musicOn = snailBait.musicCheckboxElement.checked;
    if(snailBait.musicOn){
      snailBait.musicElement.play();
    }
    else{
      snailBait.musicElement.pause();
    }
  });

snailBait.soundCheckboxElement.addEventListener('change',
  function(e){
    snailBait.soundOn = snailBait.soundCheckboxElement.checked;
  });
snailBait.initializeImages();
snailBait.createSprites();
snailBait.createAudioChannels();

snailBait.detectMobile();

if (snailBait.mobile) {
   snailBait.DEFAULT_RUNNING_SLOWLY_THRESHOLD = 30; // fps

   snailBait.instructionsElement = 
      document.getElementById('snailbait-mobile-instructions');

   snailBait.addTouchEventHandlers();

   if (/android/i.test(navigator.userAgent)) {
      snailBait.cannonSound.position = 5.4;
      snailBait.coinSound.position = 4.8;
      snailBait.electricityFlowingSound.position = 0.3;
      snailBait.explosionSound.position = 2.8;
      snailBait.pianoSound.position = 3.5;
      snailBait.thudSound.position = 1.8;
   }
}

snailBait.fitScreen();
window.addEventListener("resize", snailBait.fitScreen);
window.addEventListener("orientationchange", snailBait.fitScreen);

